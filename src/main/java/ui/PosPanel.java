/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import dao.CustomerDao;
import dao.OrderCustomerDao;
import dao.ProductDao;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import model.Customer;
import model.Employee;
import model.OrderCustomer;
import model.OrderItem;
import model.Product;

/**
 *
 * @author jarew
 */
public class PosPanel extends javax.swing.JPanel implements ProductPanel.OnAddProductListener {

    private ArrayList<Product> productList;
    private ProductTableModel model;
    private OrderCustomer orcus;
    private Employee emp = null;
    private Customer customer = null;

    /**
     * Creates new form PosPanel
     */
    /* Constructor */
    public PosPanel() {
        initComponents();
        setCategory("Hot Coffee");
    }

    PosPanel(Employee emp) {
        initComponents();
        setCategory("Hot Coffee");
        this.emp = emp;
    }

    /* แสดงเมนูต่างๆตามประเภทที่รับเข้ามา */
    private void setCategory(String cetegory) {
        clearPanel();
        ProductDao dao = new ProductDao();
        productList = dao.getByCategory(cetegory);
        int productSize = productList.size();
        productsPanel.setLayout(new GridLayout((productSize / 3) + (productSize % 3), 3));
        for (Product product : productList) {
            ProductPanel p = new ProductPanel(product);
            p.addOnAddProductListener(this);
            productsPanel.add(p);
        }
    }

    /* เคลียร์หน้าเมนู */
    private void clearPanel() {
        /* clear panel */
        productsPanel.removeAll();
        productsPanel.revalidate();
        productsPanel.repaint();
    }

    /* เพิ่มสินค้าใส่รายการสินค้า Override มาจาก ProductPanel*/
    @Override
    public void add(Product product, int amount) {
        if (orcus == null) {
            orcus = new OrderCustomer(null, 0, 0, 0, this.emp);
            orcus.addOrderItem(product, amount);
        } else {
            orcus.addOrderItem(product, amount);
        }

        loadTable(amount);
        updatePrice();
        updateNumOfProduct();
    }

    /* ดึงข้อมูลในรายการสินค้ามาแสดงบนตาราง พร้อมทั้งจำนวน*/
    public void loadTable(int amount) {
        model = new ProductTableModel(orcus.getOrderItem(), amount);
        tbOrder.setModel(model);
    }

    /* รีโหลดตาราง */
    public void reloadTable() {
        if (orcus == null) {
            tbOrder.setModel(new ProductTableModel());
        } else {
            model = new ProductTableModel(orcus.getOrderItem());
            tbOrder.setModel(model);
        }

    }

    /* อัพเดทจำนวนเงินรวม เงินที่รับมา เงินทอน */
    private void updatePrice() {
        NumberFormat myFormat = NumberFormat.getInstance();
        myFormat.setGroupingUsed(true);
        double income = 0;
        if (!txtInputMoney.getText().equals("")) {
            income = Double.parseDouble(txtInputMoney.getText());
        }
        income *= 1.00;
        if (orcus != null) {
            if (this.customer != null) {
                if (cbDiscount.isSelected()) {
                    txtChange.setText(myFormat.format(income - (orcus.getTotal() - 10)) + "");
                    txtTotal.setText(myFormat.format(orcus.getTotal()) + "");
                } else {
                    txtChange.setText(myFormat.format(income - orcus.getTotal()) + "");
                    txtTotal.setText(myFormat.format(orcus.getTotal()) + "");
                }

            } else {
                txtChange.setText(myFormat.format(income - orcus.getTotal()) + "");
                txtTotal.setText(myFormat.format(orcus.getTotal()) + "");
            }
        } else {
            txtChange.setText("0");
            txtTotal.setText("0");
            txtInputMoney.setText("");
            income = 0;
        }

        txtIncome.setText(myFormat.format(income));
    }

    /* แสดงจำนวนสินค้าทั้งหมดในรายการสินค้า */
    private void updateNumOfProduct() {
        if (this.orcus != null) {
            lblNumOfProduct.setText("สินค้าทั้งหมด " + (int) orcus.getAmount() + " จำนวน");
        } else {
            lblNumOfProduct.setText("สินค้าทั้งหมด " + 0 + " จำนวน");

        }
    }

    /* ตรวจสอบค่าที่รับจากช่องรับเงิน ตรวจสอบจำนวนติดลบ, ตรวจสอบว่าเป็นตัวเลข */
    private boolean checkInputMoney() {
        if (txtInputMoney.getText().equals("")) {
            return true;
        }
        try {
            if (Integer.parseInt(txtInputMoney.getText()) > 0) {
                return true;
            } else {
                JOptionPane.showMessageDialog(this,
                        "Input is worng (input <= 0).",
                        "WARNING MESSAGE",
                        JOptionPane.WARNING_MESSAGE);
                txtInputMoney.setText("");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "Input is worng (input is not numeric or input is too much).",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
            txtInputMoney.setText("");
        }

        return false;
    }

    /* ตวจสอบสมาชิกว่ามีในฐานข้อมูลหรือไม่ */
    //ยังไม่ได้ดักว่าไม่ใช่เบอร์โทร!!!
    private void checkMember() {
        customer = null;
        CustomerDao dao = new CustomerDao();
        String phone = txtPhone.getText();
        customer = dao.getByKey(phone);
        /* แสดง Dialog เมื่อไม่พบเบอร์โทรศัพท์ของสมาชิก */
        if (customer == null) {
            JOptionPane.showMessageDialog(this,
                    "Can't find the number.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
            txtInputMoney.setText("");
            txtPhone.setText("");
        } else {
            showCustomer();
        }

    }

    /* แสดงชื่อสมาชิกและคะแนนสะสม */
    private void showCustomer() {
        if (customer != null) {
            txtNameCus.setText(customer.getName());
            txtPoint.setText(customer.getPoint() + "");
            txtPhone.setText("");
        } else {
            txtNameCus.setText("-");
            txtPoint.setText("0");
            txtPhone.setText("");
        }
    }

    /* แสดงส่วนลด */
    private void calDiscount() {
        int point = this.customer.getPoint();
        /* ตรวจสอบคะแนนว่าเกินตามเงือนไขหรือไม่ */
        if (point >= 10) {
            txtDiscount.setText(10 + "");
        } else {
            txtDiscount.setText("0");
        }
    }

    /* การชำระเงิน */
    private void payment() {
        /* ตรวจสอบว่ามีรายการสินค้าแล้วหรือยัง */
        if (orcus != null) {
            double total = Double.parseDouble(txtTotal.getText().replace(",", ""));
            double income = Double.parseDouble(txtIncome.getText().replace(",", ""));
            double change = Double.parseDouble(txtChange.getText().replace(",", ""));
            //create order dao
            OrderCustomerDao dao = new OrderCustomerDao();
            /* ชะรำเงินแบบสมาชิก */
            if (customer != null) {
                /* ชำระเงินกับลูกค้าที่เป็นสมาชิก */
                double discount = 0;
                if (cbDiscount.isSelected()) {
                    discount = Double.parseDouble(txtDiscount.getText());
                    /* ลดคะแนนสะสม */
                    customer.setPoint(customer.getPoint() - 10);
                }
                this.orcus.setDiscount(discount);
                this.orcus.setCustomer(customer);
                this.orcus.setTotal(total);
                this.orcus.setIncome(income);
                this.orcus.setChange(change);
                this.orcus.setSeller(emp);
                addPoint(total);
                dao.add(orcus);
                CustomerDao cusdao = new CustomerDao();
                cusdao.update(customer);
                JOptionPane.showMessageDialog(this,
                        "Payment is successful.");
            } else {
                /* ชำระเงินลูกค้าขาจร */
                this.customer = new Customer(-1, "");
                this.orcus.setCustomer(customer);
                this.orcus.setTotal(total);
                this.orcus.setIncome(income);
                this.orcus.setChange(change);
                this.orcus.setSeller(emp);
                this.orcus.setDiscount(0);
                dao.add(orcus);
                /* แสดง dialog เมื่อชำระเงินสำเร็จ */
                JOptionPane.showMessageDialog(this,
                        "Payment is successful.");
            }
            clearAll();
        } else {
            /* แสดง dialog เมื่อกดชำระเงินแต่ยังไม่มีรายการสินค้า */
            JOptionPane.showMessageDialog(this,
                    "Can't remove all when order is empty.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
        }

    }

    /* เพิ่มคะแนนสะสม */
    private void addPoint(double total) {
        int point = (int) total / 10;
        customer.setPoint(customer.getPoint() + point);
    }

    /* เคลียร์ข้อมูล รายการสินค้า และ ข้อมูล ลูกค้าที่เป็นสมาชิก */
    private void clearAll() {
        if (customer != null) {
            customer = null;
            cbDiscount.setSelected(false);
            txtDiscount.setText("0");
            showCustomer();
        }
        this.orcus = null;
        reloadTable();
        updateNumOfProduct();
        updatePrice();
    }

    /* Table Model สำหรับแสดงข้อมูลบนตาราง*/
    private class ProductTableModel extends AbstractTableModel {

        private final ArrayList<OrderItem> data;
        String[] columName = {"Name", "Price", "Amount", "Total"};
        private int amount;

        public ProductTableModel(ArrayList<OrderItem> data) {
            this.data = data;
        }

        public ProductTableModel(ArrayList<OrderItem> order, int amount) {
            this.data = order;
            this.amount = amount;
        }

        public ProductTableModel() {
            this.data = null;
        }

        @Override
        public int getRowCount() {
            if (data == null) {
                return 0;
            }
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return 4;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            OrderItem orItem = this.data.get(rowIndex);
            if (columnIndex == 0) {
                return orItem.getProduct().getName();
            }
            if (columnIndex == 1) {
                return orItem.getProduct().getPrice();
            }
            if (columnIndex == 2) {
                return orItem.getAmount();
            }
            if (columnIndex == 3) {
                return orItem.getAmount() * orItem.getProduct().getPrice();
            }
            return "";
        }

        @Override
        public String getColumnName(int column) {
            return columName[column];
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        registerMember = new javax.swing.JDialog();
        jPanel5 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        txtTel = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        btnRegis = new javax.swing.JButton();
        btnRegis1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        sclProduct = new javax.swing.JScrollPane();
        productsPanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbOrder = new javax.swing.JTable();
        jPanel12 = new javax.swing.JPanel();
        btnDel = new javax.swing.JButton();
        btnDelAll = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtDiscount = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cbDiscount = new javax.swing.JCheckBox();
        jPanel10 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        txtIncome = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        txtChange = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        btnRegisMem = new javax.swing.JButton();
        btnCheckMem = new javax.swing.JButton();
        txtPhone = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtInputMoney = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        btnPayment = new javax.swing.JButton();
        lblNumOfProduct = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        txtPoint = new javax.swing.JLabel();
        txtNameCus = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();

        registerMember.setTitle("Register");
        registerMember.setMinimumSize(new java.awt.Dimension(400, 259));
        registerMember.setModal(true);
        registerMember.setResizable(false);

        jLabel8.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("ชื่อ :");

        txtName.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        jLabel9.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("เบอร์โทร :");

        txtTel.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTel, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTel, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        btnRegis.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        btnRegis.setText("สมัครสมาชิก");
        btnRegis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisActionPerformed(evt);
            }
        });

        btnRegis1.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        btnRegis1.setText("ยกเลิก");
        btnRegis1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegis1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnRegis, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRegis1, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRegis1)
                    .addComponent(btnRegis))
                .addContainerGap())
        );

        javax.swing.GroupLayout registerMemberLayout = new javax.swing.GroupLayout(registerMember.getContentPane());
        registerMember.getContentPane().setLayout(registerMemberLayout);
        registerMemberLayout.setHorizontalGroup(
            registerMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(registerMemberLayout.createSequentialGroup()
                .addContainerGap(54, Short.MAX_VALUE)
                .addGroup(registerMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(55, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, registerMemberLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        registerMemberLayout.setVerticalGroup(
            registerMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(registerMemberLayout.createSequentialGroup()
                .addContainerGap(45, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
        );

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        sclProduct.setAutoscrolls(true);

        productsPanel.setBackground(new java.awt.Color(255, 255, 255));
        productsPanel.setLayout(new java.awt.GridLayout(0, 1));
        sclProduct.setViewportView(productsPanel);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(sclProduct, javax.swing.GroupLayout.DEFAULT_SIZE, 750, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(sclProduct, javax.swing.GroupLayout.DEFAULT_SIZE, 841, Short.MAX_VALUE))
        );

        jLabel2.setFont(new java.awt.Font("TH SarabunPSK", 1, 36)); // NOI18N
        jLabel2.setText("รายการสินค้า");

        tbOrder.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Price", "Amount", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Double.class, java.lang.Integer.class, java.lang.Double.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tbOrder.setRowHeight(50);
        tbOrder.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        tbOrder.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        tbOrder.setShowGrid(false);
        jScrollPane1.setViewportView(tbOrder);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));

        btnDel.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        btnDel.setText("ลบรายการสินค้า");
        btnDel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });

        btnDelAll.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        btnDelAll.setText("ลบรายการสินค้าทั้งหมด");
        btnDelAll.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDelAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelAllActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnDelAll, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDel, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelAll, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("บาท");

        txtTotal.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        txtTotal.setForeground(new java.awt.Color(0, 0, 0));
        txtTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtTotal.setText("0");

        jLabel4.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("รวม");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtTotal)
                    .addComponent(jLabel4))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jLabel5.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("บาท");

        txtDiscount.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        txtDiscount.setForeground(new java.awt.Color(0, 0, 0));
        txtDiscount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtDiscount.setText("0");

        jLabel7.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("ส่วนลด");

        cbDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbDiscountActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbDiscount)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtDiscount)
                    .addComponent(jLabel7)
                    .addComponent(cbDiscount))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));

        jLabel23.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(0, 0, 0));
        jLabel23.setText("บาท");

        txtIncome.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        txtIncome.setForeground(new java.awt.Color(0, 0, 0));
        txtIncome.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtIncome.setText("0");

        jLabel25.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(0, 0, 0));
        jLabel25.setText("รับมา");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(txtIncome)
                    .addComponent(jLabel25))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));

        jLabel26.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(0, 0, 0));
        jLabel26.setText("บาท");

        txtChange.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        txtChange.setForeground(new java.awt.Color(0, 0, 0));
        txtChange.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtChange.setText("0");

        jLabel28.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(0, 0, 0));
        jLabel28.setText("ทอน");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtChange, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(txtChange)
                    .addComponent(jLabel28))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));

        btnRegisMem.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        btnRegisMem.setText("สมัครสมาชิก");
        btnRegisMem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegisMem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisMemActionPerformed(evt);
            }
        });

        btnCheckMem.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        btnCheckMem.setText("ตรวจสอบสมาชิก");
        btnCheckMem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCheckMem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckMemActionPerformed(evt);
            }
        });

        txtPhone.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        txtPhone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPhoneKeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("เบอร์โทรศัพท์ :");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPhone, javax.swing.GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCheckMem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRegisMem)
                .addGap(17, 17, 17))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPhone)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCheckMem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnRegisMem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtInputMoney.setFont(new java.awt.Font("TH SarabunPSK", 1, 36)); // NOI18N
        txtInputMoney.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtInputMoney.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtInputMoney.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtInputMoneyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtInputMoneyKeyReleased(evt);
            }
        });

        jLabel29.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(0, 0, 0));
        jLabel29.setText("รับเงินมา");

        btnPayment.setFont(new java.awt.Font("TH SarabunPSK", 1, 36)); // NOI18N
        btnPayment.setText("ชำระเงิน");
        btnPayment.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaymentActionPerformed(evt);
            }
        });

        lblNumOfProduct.setFont(new java.awt.Font("TH SarabunPSK", 1, 32)); // NOI18N
        lblNumOfProduct.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNumOfProduct.setText("สินค้าทั้งหมด 0 รายการ");

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));

        txtPoint.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        txtPoint.setForeground(new java.awt.Color(0, 0, 0));
        txtPoint.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtPoint.setText("-");

        txtNameCus.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        txtNameCus.setForeground(new java.awt.Color(0, 0, 0));
        txtNameCus.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txtNameCus.setText("-");

        jLabel30.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(0, 0, 0));
        jLabel30.setText("สมาชิก :");

        jLabel6.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("คะแนน :");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNameCus, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtPoint)
                        .addComponent(txtNameCus)
                        .addComponent(jLabel30))
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jButton2.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jButton2.setText("กาแฟร้อน");
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jButton3.setText("กาแฟเย็น");
        jButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jButton4.setText("สมูทตี้");
        jButton4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jButton5.setText("น้ำผลไม้");
        jButton5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 726, Short.MAX_VALUE)
                    .addComponent(btnPayment, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtInputMoney, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblNumOfProduct)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNumOfProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtInputMoney, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    /* ปุ่ม "ลบรายการสินค้าทั้งหมด" */
    private void btnDelAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelAllActionPerformed
        deleteOrderAll();
    }//GEN-LAST:event_btnDelAllActionPerformed
    /* ลบรายการสินค้าและรายละเอียดสินค้า */
    private void deleteOrderAll() throws HeadlessException {
        // TODO add your handling code here:
        /* ถ้ามีรายการสินค้าจะลบทั้งหมดและทำให้รายการสินค้าว่าง */
        if (orcus != null) {
            orcus.deleteOderItemAll();
            reloadTable();
            updatePrice();
            orcus = null;
            /* ถ้าไม่มีรายการสินค้าจะแสดง Dialog */
        } else {
            JOptionPane.showMessageDialog(this,
                    "Can't remove all when order is empty.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
        }
        updateNumOfProduct();
    }

    /* ปุ่ม "ลบรายการสินค้า" เลือกลบสินค้าที่ละรายการ */
    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        // TODO add your handling code here:
        int row = -1;
        row = tbOrder.getSelectedRow();
        /* รับค่าแถวที่เลือกมาเพื่อลบรายการสินค้านั้นๆ */
        if (row >= 0) {
            orcus.deleteOrderItem(row);
            reloadTable();
            if (orcus.getOrderItem().isEmpty()) {
                orcus = null;
            }
        } /* แสดง Dialog เมื่อยังไม่ได้เลือกรายการสินค้าที่จะลบ */ else {
            JOptionPane.showMessageDialog(this,
                    "Selete product order for remove.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
        }
        updatePrice();
        updateNumOfProduct();
    }//GEN-LAST:event_btnDelActionPerformed
    /* ช่องรับจำนวนเงิน */
    private void txtInputMoneyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInputMoneyKeyReleased
        // TODO add your handling code here:
        if (checkInputMoney()) {
            updatePrice();
        }
    }//GEN-LAST:event_txtInputMoneyKeyReleased
    /* ช่องรับจำนวนเงิน */
    private void txtInputMoneyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInputMoneyKeyPressed
        // TODO add your handling code here:
        if (checkInputMoney()) {
            updatePrice();
        }
    }//GEN-LAST:event_txtInputMoneyKeyPressed
    /* ปุ่ม "ชำระเงิน" */
    private void btnPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaymentActionPerformed
        // TODO add your handling code here:
        /* แสดง Dialog เมื่อยังไม่มีรายการสินค้าแล้วยังเสือกกดชำระเงิน */
        if (orcus == null) {
            JOptionPane.showMessageDialog(this,
                    "There are no products order.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
        } else {
            //ตรวจสอบเงินทอนติดลบหรือไม่
            if (Integer.parseInt(txtChange.getText().replace(",", "")) < 0) {
                JOptionPane.showMessageDialog(this,
                        "The income money is not enough.",
                        "WARNING MESSAGE",
                        JOptionPane.WARNING_MESSAGE);
            } else {
                payment();
            }
        }
    }//GEN-LAST:event_btnPaymentActionPerformed
    /* ปุ่ม "ตรวจสอบสมาชิก" */
    private void btnCheckMemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckMemActionPerformed
        if (checkInputMem()) {
            checkMember();
            if (this.customer != null) {
                calDiscount();
            }
        }
    }//GEN-LAST:event_btnCheckMemActionPerformed
    /* ปุ่ม "สมัครสมาชิก" */
    private void btnRegisMemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisMemActionPerformed
        // TODO add your handling code here:
        registerMember.setLocationRelativeTo(null);
        registerMember.setVisible(true);
    }//GEN-LAST:event_btnRegisMemActionPerformed
    /* แสดงเมนูเป็น กาแฟเย็น */
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        setCategory("Iced Coffee");
    }//GEN-LAST:event_jButton3ActionPerformed
    /* เช็คบล็อคสำหรับการใช้ส่วนลด */
    private void cbDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbDiscountActionPerformed
        // TODO add your handling code here:
        /* แสดง Dialog เมื่อยังไม่มีการยืนยันสมาชิก */
        if (this.customer == null) {
            JOptionPane.showMessageDialog(this,
                    "There are no members yet.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
            cbDiscount.setSelected(false);
        } else if (txtDiscount.getText().equals("0")) {
            /* แสดง Dialog เมื่อคะแนนสะสมของสมาชิกไม่เพียงพอ */
            JOptionPane.showMessageDialog(this,
                    "Not enough point.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
            cbDiscount.setSelected(false);
        } else {
            updatePrice();
        }
    }//GEN-LAST:event_cbDiscountActionPerformed
    /* แสดงเมนูเป็น กาแฟร้อน */
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        setCategory("Hot Coffee");
    }//GEN-LAST:event_jButton2ActionPerformed
    /* แสดงเมนูเป็น น้ำผลไม้ */
    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        setCategory("Juice");
    }//GEN-LAST:event_jButton5ActionPerformed
    /* แสดงเมนูเป็น สมูทตี้ */
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        setCategory("Smoothie");
    }//GEN-LAST:event_jButton4ActionPerformed

    private void btnRegisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisActionPerformed
        registerMem();
    }//GEN-LAST:event_btnRegisActionPerformed
    /* ตรวจสอบการสมัครสามาชิก */
    private void registerMem() throws HeadlessException {
        // TODO add your handling code here:
        if (txtName.getText().equals("") && txtTel.getText().equals("")) {
            JOptionPane.showMessageDialog(this,
                    "Plase input name and phone number.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
        } else if (txtName.getText().equals("")) {
            JOptionPane.showMessageDialog(this,
                    "Plase input name.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
        } else if (txtTel.getText().equals("")) {
            JOptionPane.showMessageDialog(this,
                    "Plase input phone number.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
        } else {
            String tel = txtTel.getText();
            //ตรวจว่าเป็นเบอร์โทรศัพท์หรือไม่
            if (tel.matches("-?\\d+")) {
                //ตรวจสอบว่าเกิน 10 หลัก
                if (tel.length() > 10) {
                    JOptionPane.showMessageDialog(this,
                            "Input phone number is worng input length < 10.",
                            "WARNING MESSAGE",
                            JOptionPane.WARNING_MESSAGE);
                    txtTel.setText("");
                } else {
                    //ตรวจว่ามีสมาชิกแล้วหรือยัง
                    CustomerDao dao = new CustomerDao();
                    Customer cusCheck = dao.getByKey(tel);
                    if (cusCheck != null) {
                        JOptionPane.showMessageDialog(this,
                                "This customer already exists.",
                                "WARNING MESSAGE",
                                JOptionPane.WARNING_MESSAGE);
                        txtTel.setText("");
                        txtName.setText("");
                    } else {
                        Customer newCus = new Customer(txtName.getText(), tel);
                        int id = dao.add(newCus);
                        clearAll();
                        this.customer = dao.getById(id);
                        closeRegis();
                        showCustomer();
                    }
                }
            } else {
                JOptionPane.showMessageDialog(this,
                        "Input phone number is worng.",
                        "WARNING MESSAGE",
                        JOptionPane.WARNING_MESSAGE);
                txtTel.setText("");
            }
        }
    }

    private void btnRegis1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegis1ActionPerformed
        closeRegis();
    }//GEN-LAST:event_btnRegis1ActionPerformed

    private void txtPhoneKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPhoneKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPhoneKeyTyped
    /* ตวรจสอบการรับค่า */
    private boolean checkInputMem() throws HeadlessException {
        // TODO add your handling code here:
        String tel = txtPhone.getText();
        //ตรวจว่าเป็นเบอร์โทรศัพท์หรือไม่
        if (tel.matches("-?\\d+")) {
            //ตรวจสอบว่าเกิน 10 หลัก
            if (tel.length() > 10) {
                JOptionPane.showMessageDialog(this,
                        "Input phone number is worng input length < 10.",
                        "WARNING MESSAGE",
                        JOptionPane.WARNING_MESSAGE);
                txtPhone.setText("");
            } else {
                return true;
            }
        } else {
            JOptionPane.showMessageDialog(this,
                    "Input phone number is worng.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
            txtPhone.setText("");
        }
        return false;
    }
    /* ปิกหน้าต่างสมัครสมาชิก */
    private void closeRegis() {
        // TODO add your handling code here:
        registerMember.dispose();
        txtName.setText("");
        txtTel.setText("");
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheckMem;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnDelAll;
    private javax.swing.JButton btnPayment;
    private javax.swing.JButton btnRegis;
    private javax.swing.JButton btnRegis1;
    private javax.swing.JButton btnRegisMem;
    private javax.swing.JCheckBox cbDiscount;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblNumOfProduct;
    private javax.swing.JPanel productsPanel;
    private javax.swing.JDialog registerMember;
    private javax.swing.JScrollPane sclProduct;
    private javax.swing.JTable tbOrder;
    private javax.swing.JLabel txtChange;
    private javax.swing.JLabel txtDiscount;
    private javax.swing.JLabel txtIncome;
    private javax.swing.JTextField txtInputMoney;
    private javax.swing.JTextField txtName;
    private javax.swing.JLabel txtNameCus;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JLabel txtPoint;
    private javax.swing.JTextField txtTel;
    private javax.swing.JLabel txtTotal;
    // End of variables declaration//GEN-END:variables

}
