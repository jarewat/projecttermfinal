/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import dao.EmployeeDao;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Employee;

/**
 * /**
 *
 * @author Administrator
 */
public class EmployeePanel extends javax.swing.JPanel {

   
    private ArrayList<Employee> EMList;
    private EmployeeTableModel model;
    private Employee employee;

    public EmployeePanel() {
        initComponents();
        EmployeeDao dao = new EmployeeDao();
        loadTable(dao);
        closeButton();
    }

    public void loadTable(EmployeeDao dao) {
        EMList = dao.getAll();
        model = new EmployeeTableModel(EMList);
        tblEmp.setModel(model);

    }

    public void setTable() {
        model = new EmployeeTableModel(EMList);
        tblEmp.setModel(model);

    }

    public void setDialogEdit() {
        if (tblEmp.getSelectedRow() >= 0) {
            lblIDEM.setText("" + employee.getId());
            txtEditName.setText(employee.getName());
            txtEditTel.setText(employee.getTel());
            cbEditRole.setSelectedItem(employee.getRole());
        }
    }

    public void setDialogDelete() {
        int choice = JOptionPane.showOptionDialog(null,
                "You want to delete?\n"
                + "Employee ID: " + employee.getId() + "\n"
                + "Employee Name: " + employee.getName() + "\n",
                "Delete?",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null, null, null);
        if (choice == JOptionPane.YES_OPTION) {
            emDelete();
        }

    }

    public void emDelete() {
        EmployeeDao dao = new EmployeeDao();
        if (tblEmp.getSelectedRow() >= 0) {
            dao.delete(employee.getId());
        }
    }

    public void refreshTable() {
        EmployeeDao dao = new EmployeeDao();
        ArrayList<Employee> newList = dao.getAll();
        EMList.clear();
        EMList.addAll(newList);
        model = new EmployeeTableModel(EMList);
        tblEmp.setModel(model);
        tblEmp.revalidate();
        tblEmp.repaint();
    }

    public void cleartxt() {
        txtAddName.setText("");
        txtAddTel.setText("");

    }

    public void closeButton() {
        btnEditEm.setEnabled(false);
        btnDeleteEm.setEnabled(false);
        btnClear.setEnabled(false);

    }

    public void openButton() {
        btnEditEm.setEnabled(true);
        btnDeleteEm.setEnabled(true);
    }

    public void loadFormToEMEdit() {
        employee.setName(txtEditName.getText());
        employee.setTel(txtEditTel.getText());
        employee.setRole((String) cbEditRole.getSelectedItem());

    }


    public void searchTable(EmployeeDao dao) {
        String search = txtName.getText();
        EMList = dao.getByName(search);
        if (EMList.isEmpty()) {
            JOptionPane.showMessageDialog(this, "No data found !!");
            loadTable(dao);
        } else {
            model = new EmployeeTableModel(EMList);
            tblEmp.setModel(model);
        }
        closeButton();

    }

    public void loadEmployeeToForm() {
        lblIDEM.setText("" + employee.getId());
        txtEditName.setText(employee.getName());
        txtEditTel.setText("" + employee.getTel());
      
    }

    public void loadFormToEmployee() {
        employee.setName(txtEditName.getText());
        employee.setTel(txtEditTel.getText());
        employee.setRole((String) cbEditRole.getSelectedItem());
    }

    public void loadFormToEmployeeAdd() {
        employee = new Employee();
         employee.setName(txtAddName.getText());
        employee.setTel(txtAddTel.getText());
        employee.setRole((String) cbAddRole.getSelectedItem());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        DialogEditEm = new javax.swing.JDialog();
        btnCancelE6 = new javax.swing.JButton();
        btnAcceptE6 = new javax.swing.JButton();
        txtEditTel = new javax.swing.JTextField();
        txtEditName = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        lblIDEM = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        cbEditRole = new javax.swing.JComboBox<>();
        DialogAddEm = new javax.swing.JDialog();
        btnCancelA = new javax.swing.JButton();
        btnAcceptA = new javax.swing.JButton();
        txtAddTel = new javax.swing.JTextField();
        txtAddName = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        cbAddRole = new javax.swing.JComboBox<>();
        jPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        btnSearchEm = new javax.swing.JButton();
        btnAddEm = new javax.swing.JButton();
        btnEditEm = new javax.swing.JButton();
        btnDeleteEm = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblEmp = new javax.swing.JTable();

        DialogEditEm.setTitle("แก้ไขข้อมูลพนักงาน");
        DialogEditEm.setMinimumSize(new java.awt.Dimension(400, 306));

        btnCancelE6.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        btnCancelE6.setText("ยกเลิก");
        btnCancelE6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelE6ActionPerformed(evt);
            }
        });

        btnAcceptE6.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        btnAcceptE6.setText("ยืนยัน");
        btnAcceptE6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAcceptE6ActionPerformed(evt);
            }
        });

        txtEditTel.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N

        txtEditName.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N

        jLabel28.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        jLabel28.setText("้เบอร์โทรศัพท์ :");

        jLabel27.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        jLabel27.setText("ชื่อ-นามสกุล  :");

        jLabel26.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        jLabel26.setText("รหัสพนักงาน :");

        lblIDEM.setText("ID_Employee");

        jLabel30.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        jLabel30.setText("ประเภท :");

        cbEditRole.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        cbEditRole.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Staff", "Manager" }));

        javax.swing.GroupLayout DialogEditEmLayout = new javax.swing.GroupLayout(DialogEditEm.getContentPane());
        DialogEditEm.getContentPane().setLayout(DialogEditEmLayout);
        DialogEditEmLayout.setHorizontalGroup(
            DialogEditEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DialogEditEmLayout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addGroup(DialogEditEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(DialogEditEmLayout.createSequentialGroup()
                        .addComponent(btnAcceptE6, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelE6, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(DialogEditEmLayout.createSequentialGroup()
                        .addGroup(DialogEditEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(DialogEditEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel28)
                                .addGroup(DialogEditEmLayout.createSequentialGroup()
                                    .addComponent(jLabel26)
                                    .addGap(2, 2, 2))
                                .addComponent(jLabel30))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DialogEditEmLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 1, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(10, 10, 10)
                        .addGroup(DialogEditEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtEditName)
                            .addComponent(cbEditRole, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtEditTel, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblIDEM, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(77, 77, 77))
        );
        DialogEditEmLayout.setVerticalGroup(
            DialogEditEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DialogEditEmLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(DialogEditEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIDEM))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(DialogEditEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(txtEditName))
                .addGap(18, 18, 18)
                .addGroup(DialogEditEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(txtEditTel))
                .addGap(18, 18, 18)
                .addGroup(DialogEditEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(cbEditRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(DialogEditEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAcceptE6)
                    .addComponent(btnCancelE6))
                .addGap(40, 40, 40))
        );

        DialogAddEm.setTitle("เพิ่มข้อมูลพนักงาน");
        DialogAddEm.setMinimumSize(new java.awt.Dimension(400, 301));

        btnCancelA.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        btnCancelA.setText("ยกเลิก");
        btnCancelA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelAActionPerformed(evt);
            }
        });

        btnAcceptA.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        btnAcceptA.setText("ยืนยัน");
        btnAcceptA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAcceptAActionPerformed(evt);
            }
        });

        txtAddTel.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N

        txtAddName.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N

        jLabel31.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel31.setText("เบอร์โทรศัพท์ :");

        jLabel29.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel29.setText("ชื่อ-นามสกุล  :");

        jLabel32.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel32.setText("ประเภท :");

        cbAddRole.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        cbAddRole.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Staff", "Manager" }));

        javax.swing.GroupLayout DialogAddEmLayout = new javax.swing.GroupLayout(DialogAddEm.getContentPane());
        DialogAddEm.getContentPane().setLayout(DialogAddEmLayout);
        DialogAddEmLayout.setHorizontalGroup(
            DialogAddEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DialogAddEmLayout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addGroup(DialogAddEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(DialogAddEmLayout.createSequentialGroup()
                        .addComponent(btnAcceptA, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53)
                        .addComponent(btnCancelA, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(DialogAddEmLayout.createSequentialGroup()
                        .addGroup(DialogAddEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel32, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel29, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel31, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(DialogAddEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtAddTel, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtAddName, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbAddRole, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(69, 69, 69))
        );
        DialogAddEmLayout.setVerticalGroup(
            DialogAddEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DialogAddEmLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(DialogAddEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(txtAddName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(DialogAddEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel31)
                    .addComponent(txtAddTel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(DialogAddEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbAddRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32))
                .addGap(20, 20, 20)
                .addGroup(DialogAddEmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelA, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAcceptA, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(67, 67, 67))
        );

        setBackground(new java.awt.Color(238, 215, 197));
        setMinimumSize(new java.awt.Dimension(1050, 600));
        setPreferredSize(new java.awt.Dimension(1050, 600));

        jPanel.setBackground(new java.awt.Color(238, 215, 197));

        jLabel1.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("ชื่อ-นามสกุล :");

        txtName.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        txtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNameActionPerformed(evt);
            }
        });

        btnSearchEm.setBackground(new java.awt.Color(247, 135, 49));
        btnSearchEm.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        btnSearchEm.setForeground(new java.awt.Color(255, 255, 255));
        btnSearchEm.setText("ค้นหา");
        btnSearchEm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchEmActionPerformed(evt);
            }
        });

        btnAddEm.setBackground(new java.awt.Color(247, 135, 49));
        btnAddEm.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        btnAddEm.setForeground(new java.awt.Color(255, 255, 255));
        btnAddEm.setText("เพิ่มข้อมูลพนักงาน");
        btnAddEm.setFocusPainted(false);
        btnAddEm.setMaximumSize(new java.awt.Dimension(150, 30));
        btnAddEm.setMinimumSize(new java.awt.Dimension(150, 30));
        btnAddEm.setPreferredSize(new java.awt.Dimension(150, 30));
        btnAddEm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddEmActionPerformed(evt);
            }
        });

        btnEditEm.setBackground(new java.awt.Color(247, 135, 49));
        btnEditEm.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        btnEditEm.setForeground(new java.awt.Color(255, 255, 255));
        btnEditEm.setText("แก้ไข");
        btnEditEm.setMaximumSize(new java.awt.Dimension(65, 30));
        btnEditEm.setMinimumSize(new java.awt.Dimension(65, 30));
        btnEditEm.setPreferredSize(new java.awt.Dimension(65, 30));
        btnEditEm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditEmActionPerformed(evt);
            }
        });

        btnDeleteEm.setBackground(new java.awt.Color(247, 135, 49));
        btnDeleteEm.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        btnDeleteEm.setForeground(new java.awt.Color(255, 255, 255));
        btnDeleteEm.setText("ลบ");
        btnDeleteEm.setMaximumSize(new java.awt.Dimension(50, 30));
        btnDeleteEm.setMinimumSize(new java.awt.Dimension(50, 30));
        btnDeleteEm.setPreferredSize(new java.awt.Dimension(50, 30));
        btnDeleteEm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteEmActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(247, 135, 49));
        btnClear.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        btnClear.setForeground(new java.awt.Color(255, 255, 255));
        btnClear.setText("เคลียรค้นหา");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelLayout = new javax.swing.GroupLayout(jPanel);
        jPanel.setLayout(jPanelLayout);
        jPanelLayout.setHorizontalGroup(
            jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(20, 20, 20)
                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSearchEm)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnClear)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 291, Short.MAX_VALUE)
                .addComponent(btnEditEm, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnDeleteEm, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAddEm, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanelLayout.setVerticalGroup(
            jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDeleteEm, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEditEm, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearchEm, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddEm, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );

        tblEmp.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        tblEmp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblEmp.setMaximumSize(new java.awt.Dimension(300, 200));
        tblEmp.setMinimumSize(new java.awt.Dimension(300, 200));
        tblEmp.setRowHeight(50);
        tblEmp.setSelectionBackground(new java.awt.Color(247, 135, 49));
        tblEmp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblEmpMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tblEmp);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2)
                    .addComponent(jPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnEditEmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditEmActionPerformed
        DialogEditEm.setLocationRelativeTo(null);
        DialogEditEm.setVisible(true);
        setDialogEdit();
    }//GEN-LAST:event_btnEditEmActionPerformed

    private void btnDeleteEmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteEmActionPerformed

        setDialogDelete();
        refreshTable();
    }//GEN-LAST:event_btnDeleteEmActionPerformed

    private void btnAcceptE6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAcceptE6ActionPerformed
        loadFormToEmployee();
        EmployeeDao dao = new EmployeeDao();
        dao.update(employee);
        refreshTable();
        closeButton();
        DialogEditEm.setVisible(false);
    }//GEN-LAST:event_btnAcceptE6ActionPerformed

    private void btnAcceptAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAcceptAActionPerformed
        EmployeeDao dao = new EmployeeDao();
        EMList = dao.getAll();
        if (txtAddName.getText().equals("")) {
            JOptionPane.showMessageDialog(this,
                    "Please insert name in the text box.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
        } else if (txtAddTel.getText().equals("")) {
            JOptionPane.showMessageDialog(this,
                    "Please insert phone number in the text box.",
                    "WARNING MESSAGE",
                    JOptionPane.WARNING_MESSAGE);

        } else {

            String name = txtAddName.getText();
            Pattern pt1 = Pattern.compile("^[a-zA-Zก-ฮะ-์0-9]+([\\s]+[a-zA-Z]+)*$");
            Matcher mh1 = pt1.matcher(name);
            boolean nameMatcherFound = mh1.matches();
            String tel = txtAddTel.getText();
            boolean telMatcherFound = tel.matches("-?\\d+");
            boolean checkEM = true;
            if (nameMatcherFound == true && telMatcherFound == true) {
                for (Employee employee : EMList) {
                    if (employee.getTel().equals(txtAddTel.getText())) {
                        checkEM = false;
                        break;
                    }
                }
                if (checkEM) {
                    loadFormToEmployeeAdd();
                    dao.add(employee);
                    refreshTable();
                    cleartxt();
                    DialogAddEm.setVisible(false);

                } else {
                    JOptionPane.showMessageDialog(this,
                            "This user is already in the database, please change the username.",
                            "WARNING MESSAGE",
                            JOptionPane.WARNING_MESSAGE);
                    refreshTable();
                    txtAddName.setText("");
                }
            } else {
                JOptionPane.showMessageDialog(this,
                        "Please enter only letters or numbers.",
                        "WARNING MESSAGE",
                        JOptionPane.WARNING_MESSAGE);
                if (!nameMatcherFound) {
                    txtAddName.setText("");
                }
                if (!telMatcherFound) {
                    txtAddTel.setText("");
                }
            }
        }

    }//GEN-LAST:event_btnAcceptAActionPerformed

    private void btnAddEmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddEmActionPerformed
        DialogAddEm.setLocationRelativeTo(null);
        DialogAddEm.setVisible(true);
    }//GEN-LAST:event_btnAddEmActionPerformed

    private void btnSearchEmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchEmActionPerformed
        EmployeeDao dao = new EmployeeDao();
        searchTable(dao);
        btnClear.setEnabled(true);
    }//GEN-LAST:event_btnSearchEmActionPerformed

    private void txtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNameActionPerformed

    private void btnCancelE6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelE6ActionPerformed
        DialogEditEm.setVisible(false);
    }//GEN-LAST:event_btnCancelE6ActionPerformed

    private void btnCancelAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelAActionPerformed
        DialogAddEm.setVisible(false);
    }//GEN-LAST:event_btnCancelAActionPerformed

    private void tblEmpMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEmpMousePressed
        if(tblEmp.getSelectedRow() != 1){
           employee = EMList.get(tblEmp.getSelectedRow());
        openButton();
        }
    }//GEN-LAST:event_tblEmpMousePressed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        txtName.setText("");
        refreshTable();
        btnClear.setEnabled(false);
    }//GEN-LAST:event_btnClearActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDialog DialogAddEm;
    private javax.swing.JDialog DialogEditEm;
    private javax.swing.JButton btnAcceptA;
    private javax.swing.JButton btnAcceptE6;
    private javax.swing.JButton btnAddEm;
    private javax.swing.JButton btnCancelA;
    private javax.swing.JButton btnCancelE6;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDeleteEm;
    private javax.swing.JButton btnEditEm;
    private javax.swing.JButton btnSearchEm;
    private javax.swing.JComboBox<String> cbAddRole;
    private javax.swing.JComboBox<String> cbEditRole;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JPanel jPanel;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblIDEM;
    private javax.swing.JTable tblEmp;
    private javax.swing.JTextField txtAddName;
    private javax.swing.JTextField txtAddTel;
    private javax.swing.JTextField txtEditName;
    private javax.swing.JTextField txtEditTel;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables

    private static class EmployeeTableModel extends AbstractTableModel {

        private ArrayList<Employee> data;
        String[] columnName = {"ID", "Name", "Phone number", "Role"};

        private EmployeeTableModel(ArrayList<Employee> data) {
            this.data = data;
        }

        @Override
        public int getRowCount() {
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return 4;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Employee employee = this.data.get(rowIndex);
            if (columnIndex == 0) {
                return employee.getId();
            }
            if (columnIndex == 1) {
                return employee.getName();
            }
            if (columnIndex == 2) {
                return employee.getTel();
            }
            if (columnIndex == 3) {
                return employee.getRole();
            }

            return "";

        }

        @Override
        public String getColumnName(int column) {
            return columnName[column];
        }
    }
}
