/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.Customer;
import model.Employee;
import model.OrderCustomer;
import model.OrderItem;
import model.Product;

/**
 *
 * @author Waratchapon Ponpiya
 */
public class OrderCustomerDao implements DaoInterface<OrderCustomer> {

    @Override
    public int add(OrderCustomer object) {

        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Order_Customer (cus_id,seller_id,or_cus_total,or_cus_income,or_cus_change,or_cus_discount) VALUES (?,?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getId());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            stmt.setDouble(4, object.getIncome());
            stmt.setDouble(5, object.getChange());
            stmt.setDouble(6, object.getDiscount());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (OrderItem i : object.getOrderItem()) {
                String sqlItem = "INSERT INTO Order_Item (or_cus_id,pd_id,or_item_price,or_item_amount) VALUES (?,?,?,?);";
                PreparedStatement stmtItem = conn.prepareStatement(sqlItem);
                stmtItem.setDouble(1, i.getOrdercustomer().getId());
                stmtItem.setInt(2, i.getProduct().getId());
                stmtItem.setDouble(3, i.getPrice());
                stmtItem.setInt(4, i.getAmount());
                int rowItem = stmtItem.executeUpdate();
                ResultSet resultItem = stmt.getGeneratedKeys();

            }

        } catch (SQLException ex) {
            //createdDialogbox
            System.out.println("Failed to created OrderItem !!");

        }
        db.close();
        return id;

    }

    @Override
    public ArrayList<OrderCustomer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT or_cus_id ,\n"
                    + "       or_cus_date, \n"
                    + "       cus_id ,\n"
                    + "       \n"
                    + "       seller_id,\n"
                    + "       \n"
                    + "       or_cus_total, \n"
                    + "       or_cus_income, \n"
                    + "       or_cus_change,\n"
                    + "       or_cus_discount \n"
                    + "FROM Order_Customer ORDER BY or_cus_date DESC\n"
                    + ";";

            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("or_cus_id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("or_cus_date"));
                int customerId = result.getInt("cus_id");
                //String customerName = result.getString("cus_name");
                int sellerID = result.getInt("seller_id");
                //String userName = result.getString("user_name")
                double totalPrice = result.getDouble("or_cus_total");
                double incomePrice = result.getDouble("or_cus_income");
                double changePrice = result.getDouble("or_cus_change");
                double discountPrice = result.getDouble("or_cus_discount");
                OrderCustomer ordercustomer = new OrderCustomer(id,
                        new Customer(customerId, null),
                        created, totalPrice, incomePrice, changePrice, discountPrice,
                        new Employee(sellerID));
                list.add(ordercustomer);

            }

        } catch (SQLException ex) {
            System.out.println("Error:Unable to select all ordercustomer!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all ordercustomer!!" + ex.getMessage());
        }
        db.close();
        return list;

    }

    @Override
    public OrderCustomer getById(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT or_cus_id ,\n"
                    + "       or_cus_date, \n"
                    + "       cus_id ,\n"
                    + "       seller_id,\n"
                    + "       or_cus_total, \n"
                    + "       or_cus_income, \n"
                    + "       or_cus_change,\n"
                    + "       or_cus_discount \n"
                    + "FROM Order_Customer \n"
                    + "WHERE or_cus_id=?;";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                int rid = result.getInt("or_cus_id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("or_cus_date"));
                int customerId = result.getInt("cus_id");
//                String customerName = result.getString("cus_name");
                int sellerID = result.getInt("seller_id");
//                String userName = result.getString("user_name");
                double totalPrice = result.getDouble("or_cus_total");
                double incomePrice = result.getDouble("or_cus_income");
                double changePrice = result.getDouble("or_cus_change");
                double discountPrice = result.getDouble("or_cus_discount");
                OrderCustomer ordercustomer = new OrderCustomer(rid,
                        new Customer(customerId, ""),
                        created, totalPrice, incomePrice, changePrice, discountPrice,
                        new Employee(sellerID));
                getOrderDetail(conn, id, ordercustomer);
                db.close();
                return ordercustomer;
            }

        } catch (SQLException ex) {
            System.out.println("Error:Unable to select all receipt id" + id + "!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all ordercustomer!!" + ex.getMessage());
        }
        db.close();
        return null;
    }

    private void getOrderDetail(Connection con, int id, OrderCustomer order) throws SQLException {
        String sqlDetail = "SELECT or_item_id,\n"
                + "       p.pd_id as pd_id,\n"
                + "       p.pd_name as pd_name,\n"
                + "       p.pd_category as pd_category, \n"
                + "       or_item_amount,\n"
                + "       or_item_price,\n"
                + "       or_cus_id\n"
                + "  FROM Order_Item o,Product p \n"
                + "  WHERE or_cus_id = ? AND o.pd_id = p.pd_id ;";
        PreparedStatement stmtDetail = con.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();

        while (resultDetail.next()) {
            int orderDetailId = resultDetail.getInt("or_item_id");
            int productId = resultDetail.getInt("pd_id");
            String productName = resultDetail.getString("pd_name");
            String categoryProduct = resultDetail.getString("pd_category");
            int amount = resultDetail.getInt("or_item_amount");
            double price = resultDetail.getDouble("or_item_price");
            int ordercusId = resultDetail.getInt("or_cus_id");
            Product product = new Product(productId, productName, price, categoryProduct);
            order.addOrderItem(orderDetailId, product, amount, price);
        }
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            /* delete order*/
            String sql = "DELETE FROM Order_Customer WHERE or_cus_id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            /* delete order of detail */
            sql = "DELETE FROM Order_Item WHERE or_cus_id = ?;";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("ERROR : Unable to delete Ordercustomer id " + id + "!!" + ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(OrderCustomer object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Product getBestSellerByCatagory(String key) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT  pd_id,pd_name ,pd_price,pd_category, pd_image  FROM Product WHERE  ( SELECT  count(pd_id) FROM Order_Item group by pd_id) and pd_category =" + " \"" + key + "\"" + "Limit 1;";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("pd_id");
                String name = result.getString("pd_name");
                double price = result.getDouble("pd_price");
                String category = result.getString("pd_category");
                String image = result.getString("pd_image");
                Product product = new Product(pid, name, price, category, image);
                db.close();
                return product;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : " + ex);
        }
        db.close();
        return null;

    }

    public double getByYear(String key) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        double price = 0;
        try {
            String sql = "SELECT or_cus_total FROM Order_Customer where or_cus_date like" + " \'" + key + "%" + "\';";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                price += result.getDouble("or_cus_total");
            }
            db.close();
            return price;
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL" + ex);
        }
        db.close();
        return 0;

    }

    public double getByMonth(String key) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        double price = 0;
        try {
            String sql = "SELECT or_cus_total FROM Order_Customer where or_cus_date like" + " \'%-" + key + "-%" + "\';";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                price += result.getDouble("or_cus_total");
            }
            db.close();
            return price;
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL" + ex);
        }
        db.close();
        return 0;

    }

    public Customer getNameByAmountBuy() {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();

        try {
            String sql = "SELECT or_cus_id,\n"
                    + "       or_cus_date,\n"
                    + "       or_cus_income,\n"
                    + "       or_cus_total,\n"
                    + "       or_cus_discount,\n"
                    + "       or_cus_change,\n"
                    + "       seller_id,\n"
                    + "       o.cus_id as cus_id,\n"
                    + "       c.cus_name as cus_name,\n"
                    + "       sum(o.or_cus_total) as total\n"
                    + "  FROM Order_Customer o , Customer c\n"
                    + "  WHERE o.cus_id = c.cus_id\n"
                    + "  GROUP by o.cus_id\n"
                    + "  ORDER by total DESC\n"
                    + "  LIMIT 1;";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int id = result.getInt("cus_id");
                String name = result.getString("cus_name");
                Customer customer = new Customer(id, name, null, 0);
                db.close();
                return customer;
            }

        } catch (SQLException ex) {
            System.out.println("ERROR : SQL" + ex);
        }
        db.close();
        return null;

    }

    public double getTotalByAmountBuy() {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();

        try {
            String sql = "SELECT or_cus_id,\n"
                    + "       or_cus_date,\n"
                    + "       or_cus_income,\n"
                    + "       or_cus_total,\n"
                    + "       or_cus_discount,\n"
                    + "       or_cus_change,\n"
                    + "       seller_id,\n"
                    + "       o.cus_id as cus_id,\n"
                    + "       c.cus_name as cus_name,\n"
                    + "       sum(o.or_cus_total) as total\n"
                    + "  FROM Order_Customer o , Customer c\n"
                    + "  WHERE o.cus_id = c.cus_id\n"
                    + "  GROUP by o.cus_id\n"
                    + "  ORDER by total DESC\n"
                    + "  LIMIT 1;";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                double total = result.getDouble("total");
                db.close();
                return total;
            }

        } catch (SQLException ex) {
            System.out.println("ERROR : SQL" + ex);
        }
        db.close();
        return 0;

    }

    public double getTotalByDate(String key) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();

        try {
            String sql = "SELECT or_cus_id,\n"
                    + "        strftime('%d/%m/%Y', or_cus_date) as date,\n"
                    + "       or_cus_income,\n"
                    + "       sum(or_cus_total) as total,\n"
                    + "       or_cus_discount,\n"
                    + "       or_cus_change,\n"
                    + "       cus_id,\n"
                    + "       seller_id,\n"
                    + "       avg(or_cus_total) as avgTotal\n"
                    + "  FROM Order_Customer\n"
                    + "  WHERE date = " + "\' "+ key + "\';";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                double total = result.getDouble("total");
                System.out.println(key);
                System.out.println(total);
                db.close();
                return total;
            }

        } catch (SQLException ex) {
            System.out.println("ERROR : SQL" + ex);
        }
        db.close();
        return 0;

    }

}
