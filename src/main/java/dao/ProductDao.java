/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Product;

/**
 *
 * @author LIGHTsOUT
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Product (pd_name,pd_price,pd_category,pd_image)VALUES (?,?,?,?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setString(3, object.getCategory());
            stmt.setString(4, object.getImage());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return id;

    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT pd_id,pd_name,pd_price,pd_category,pd_image FROM Product";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("pd_id");
                String name = result.getString("pd_name");
                double price = result.getDouble("pd_price");
                String category = result.getString("pd_category");
                String image = result.getString("pd_image");
                Product product = new Product(id, name, price, category,image);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return list;

    }
    
    @Override
    public Product getById(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT pd_id,pd_name,pd_price,pd_category,pd_image FROM Product WHERE pd_id =" + id;
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("pd_id");
                String name = result.getString("pd_name");
                double price = result.getDouble("pd_price");
                String category = result.getString("pd_category");
                String image = result.getString("pd_category");
                Product product = new Product(pid, name, price, category,image);
                db.close();
                return product;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return null;

    }
    public ArrayList<Product> getByCategory(String key) {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT pd_id,pd_name,pd_price,pd_category,pd_image FROM Product WHERE pd_category =" +"\""+ key + "\" ;";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("pd_id");
                String name = result.getString("pd_name");
                double price = result.getDouble("pd_price");
                String category = result.getString("pd_category");
                String image = result.getString("pd_image");
                Product product = new Product(id, name, price, category,image);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return list;
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Product WHERE pd_id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return row;

    }

    @Override
    public int update(Product object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Product SET pd_name = ?,pd_price = ?,pd_category = ?,pd_image = ? WHERE pd_id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setString(3, object.getCategory());
             stmt.setString(4, object.getImage());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
        db.close();
        return row;

    }
    public ArrayList<Product> getByName(String key) {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT pd_id , pd_name , pd_price , pd_category , pd_image FROM Product WHERE pd_name LIKE" + "\'" + key + "%" + "\'" + " OR pd_name LIKE" + "\'" + "%" + key + "%" + "\'" + " OR pd_name LIKE" + "\'" + "%" + key + "\';";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("pd_id");
                String name = result.getString("pd_name");
                double price = result.getDouble("pd_price");
                String category = result.getString("pd_category");
                String image = result.getString("pd_image");
                Product product = new Product(id, name, price, category, image);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : " + ex +" 1");
        }
        db.close();
        return list;
    }
    
    public ArrayList<Product> getByNameAndType(String key, String type) {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT pd_id,pd_name,pd_price,pd_category,pd_image FROM Product WHERE " + "(" + "pd_name LIKE" + "\'" + key + "%" + "\'" + " OR pd_name LIKE" + "\'" + "%" + key + "%" + "\'" + " OR pd_name LIKE" + "\'" + "%" + key + "\'" + ")" + " AND pd_category =" + "\"" + type + "\" ;";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("pd_id");
                String name = result.getString("pd_name");
                double price = result.getDouble("pd_price");
                String category = result.getString("pd_category");
                String image = result.getString("pd_image");
                Product product = new Product(id, name, price, category, image);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : " + ex+" 2");
        }
        db.close();
        return list;
    }
    
    

}
