/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Employee;

/**
 *
 * @author Administrator
 */
public class EmployeeDao implements DaoInterface<Employee> {

    @Override
    public int add(Employee object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Employee (emp_name, emp_tel, emp_role ) VALUES (?,?,?); ";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getRole());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR :" + ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT emp_id, emp_name, emp_tel, emp_role FROM Employee;";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("emp_id");
                String name = result.getString("emp_name");
                String tel = result.getString("emp_tel");
                String role = result.getString("emp_role");
                Employee employee = new Employee(id, name, tel, role);
                list.add(employee);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR :" + ex);
        }
        db.close();
        return list;
    }

    @Override
    public Employee getById(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT emp_id, emp_name, emp_tel, emp_role FROM Employee WHERE emp_id =" + id;
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int eid = result.getInt("emp_id");
                String name = result.getString("emp_name");
                String tel = result.getString("emp_tel");
                String role = result.getString("emp_role");
                Employee employee = new Employee(eid, name, tel, role);
                db.close();
                return employee;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR :" + ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Employee WHERE emp_id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR :" + ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Employee SET emp_name = ?, emp_tel = ?, emp_role = ? WHERE emp_id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getRole());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("ERROR : " + ex);
        }
        db.close();
        return row;
    }

    public ArrayList<Employee> getByName(String key) {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT emp_id,emp_name,emp_tel,emp_role FROM Employee WHERE emp_name LIKE" + "\'" + key + "%" + "\'" + " OR emp_name LIKE" + "\'" + "%" + key + "%" + "\'" + " OR emp_name LIKE" + "\'" + "%" + key + "\';";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("emp_id");
                String name = result.getString("emp_name");
                String tel = result.getString("emp_tel");
                String role = result.getString("emp_role");
                Employee employee = new Employee(id, name, tel, role);
                list.add(employee);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : " + ex);
        }
        db.close();
        return list;
    }

}
