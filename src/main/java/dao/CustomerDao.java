/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import static java.nio.file.Files.list;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;

/**
 *
 * @author aomza
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;

        try {
            String sql = "INSERT INTO Customer (cus_name,cus_tel,cus_point)VALUES (?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPhone());
            stmt.setInt(3, object.getPoint());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: " + ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT cus_id,cus_name,cus_tel,cus_point FROM Customer";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("cus_id");
                String name = result.getString("cus_name");
                String tel = result.getString("cus_tel");
                int point = result.getInt("cus_point");
                Customer customer = new Customer(id, name, tel, point);
                list.add(customer);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: " + ex);
        }
        db.close();
        return list;
    }

    @Override
    public Customer getById(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT cus_id,cus_name,cus_tel,cus_point FROM Customer WHERE cus_id=" + id;
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int cusid = result.getInt("cus_id");
                String name = result.getString("cus_name");
                String phone = result.getString("cus_tel");
                int point = result.getInt("cus_point");
                Customer customer = new Customer(cusid, name, phone, point);
                db.close();
                return customer;

            }
        } catch (SQLException ex) {
            System.out.println("ERROR: " + ex);
        }
        db.close();
        return null;
    }

    public Customer getByKey(String Key) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT cus_id,\n"
                    + "       cus_name,\n"
                    + "       cus_tel,\n"
                    + "       cus_point FROM Customer WHERE cus_tel =" 
                    + "\"" + Key + "\" ; ";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int cusid = result.getInt("cus_id");
                String name = result.getString("cus_name");
                String phone = result.getString("cus_tel");
                int point = result.getInt("cus_point");
                Customer customer = new Customer(cusid, name, phone, point);
                db.close();
                return customer;

            }
        } catch (SQLException ex) {
            System.out.println("ERROR: " + ex);
        }
        db.close();
        return null;
    }

    public ArrayList<Customer> getNameAndTel(String key) {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT cus_id,\n"
                    + "       cus_name,\n"
                    + "       cus_tel,\n"
                    + "       cus_point FROM Customer WHERE cus_name LIKE" 
                    + "\'" + key + "%" + "\'" + " OR cus_name LIKE" + "\'" 
                    + "%" + key + "%" + "\'" + " OR cus_name LIKE" + "\'" 
                    + "%" + key + "\';";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("cus_id");
                String name = result.getString("cus_name");
                String tel = result.getString("cus_tel");
                int point = result.getInt("cus_point");
                Customer customer = new Customer(id, name, tel, point);
                list.add(customer);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: " + ex);
        }
        db.close();
        return list;
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM Customer WHERE cus_id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("ERROR: " + ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Customer SET cus_name = ?,cus_tel = ?,cus_point = ? WHERE cus_id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPhone());
            stmt.setInt(3, object.getPoint());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR: " + ex);
        }
        db.close();
        return row;
    }

}
