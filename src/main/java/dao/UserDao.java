/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Employee;
import model.User;

/**
 *
 * @author Pannawit Juntanan
 */
public class UserDao implements DaoInterface<User> {

    @Override
    public int add(User object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO User (user_username, user_pass, emp_id) VALUES (?,?,?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getUsername());
            stmt.setString(2, object.getPass());
            stmt.setInt(3, object.getEmp().getId());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : " + ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT user_id,user_username,user_pass,emp_id FROM User";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("user_id");
                String userName = result.getString("user_username");
                String pass = result.getString("user_pass");
                int emp_id = result.getInt("emp_id");
                User user = new User(id, userName, pass, new Employee(emp_id));
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : get All" + ex);
        }
        db.close();
        return list;
    }

    @Override
    public User getById(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT user_id,user_username,user_pass,emp_id FROM User WHERE user_id =" + id;
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int uid = result.getInt("user_id");
                String userName = result.getString("user_username");
                String pass = result.getString("user_pass");
                int emp_id = result.getInt("emp_id");
                User user = new User(uid, userName, pass, new Employee(emp_id));
                db.close();
                return user;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : " + ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM User WHERE user_id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : " + ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE User\n"
                    + "   SET user_username = ?,\n"
                    + "       user_pass = ?,\n"
                    + "       emp_id = ?\n"
                    + " WHERE user_id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getUsername());
            stmt.setString(2, object.getPass());
            stmt.setInt(3, object.getEmp().getId());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : " + ex);
        }
        db.close();
        return row;
    }

//    public ArrayList<User> getByType(String key) {
//        ArrayList list = new ArrayList();
//        Connection con = null;
//        Database db = Database.getInstance();
//        con = db.getConnection();
//        try {
//            String sql = "SELECT user_id , user_username , user_name , user_pass , user_role FROM User WHERE user_role =" + "\"" + key + "\" ;";
//            Statement stmt = con.createStatement();
//            ResultSet result = stmt.executeQuery(sql);
//            while (result.next()) {
//                int id = result.getInt("user_id");
//                String userName = result.getString("user_username");
//                String name = result.getString("user_name");
//                String pass = result.getString("user_pass");
//                String role = result.getString("user_role");
//                User user = new User(id, userName, name, pass, role);
//                list.add(user);
//            }
//        } catch (SQLException ex) {
//            System.out.println("ERROR : " + ex);
//        }
//        db.close();
//        return list;
//    }
    public ArrayList<User> getByUserName(String key) {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String sql = "SELECT user_id,user_username,user_pass,emp_id FROM User WHERE user_username LIKE" + "\'" + key + "%" + "\'" + " OR user_username LIKE" + "\'" + "%" + key + "%" + "\'" + " OR user_username LIKE" + "\'" + "%" + key + "\';";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("user_id");
                String userName = result.getString("user_username");
                String pass = result.getString("user_pass");
                int emp_id = result.getInt("emp_id");
                User user = new User(id, userName, pass, new Employee(emp_id));
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : " + ex);
        }
        db.close();
        return list;
    }
//    public ArrayList<User> getByNameAndRole(String key, String type) {
//        ArrayList list = new ArrayList();
//        Connection con = null;
//        Database db = Database.getInstance();
//        con = db.getConnection();
//        try {
//            String sql = "SELECT user_id,user_username,user_name,user_pass,user_role FROM User WHERE " + "(" + "user_name LIKE" + "\'" + key + "%" + "\'" + " OR user_name LIKE" + "\'" + "%" + key + "%" + "\'" + " OR user_name LIKE" + "\'" + "%" + key + "\'" + ")" + " AND user_role =" + "\"" + type + "\" ;";
//            Statement stmt = con.createStatement();
//            ResultSet result = stmt.executeQuery(sql);
//            while (result.next()) {
//                int id = result.getInt("user_id");
//                String userName = result.getString("user_username");
//                String name = result.getString("user_name");
//                String pass = result.getString("user_pass");
//                String role = result.getString("user_role");
//                User user = new User(id, userName, name, pass, role);
//                list.add(user);
//            }
//        } catch (SQLException ex) {
//            System.out.println("ERROR : " + ex);
//        }
//        db.close();
//        return list;
//    }
}
