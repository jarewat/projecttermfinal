/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Administrator
 */
public class Employee {

    private int id;
    private String name;
    private String tel;
    private String role;

    public Employee(int id, String name, String tel, String role) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.role = role;
    }

    public Employee(String name, String tel, String role) {
        this(-1, name, tel, role);
    }

    public Employee(int sellerID) {
        this(sellerID,"","","");
    }

    public Employee() {
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", tel=" + tel + '}';
    }

}
