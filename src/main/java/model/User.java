/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author jarew
 */
public class User {

    private int id;
    private String username;
    private String pass;
    private Employee emp;

    public User(int id, String username, String pass, Employee emp) {
        this.id = id;
        this.username = username;
        this.pass = pass;
        this.emp = emp;
    }

    public User(String username, String pass, Employee emp) {
        this(-1, username, pass, emp);
    }

    public User() {
    }

    public User(int sellerID) {
        this(sellerID, null, null, null);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public Employee getEmp() {
        return emp;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", pass=" + pass + ", emp=" + emp + '}';
    }

}
