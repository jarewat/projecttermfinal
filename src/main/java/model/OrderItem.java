/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author PC
 */
public class OrderItem {

    private int id;
    private Product product;
    private int amount;
    private double price;
    private OrderCustomer ordercustomer;

    public OrderItem(int id, Product product, int amount, double price, OrderCustomer ordercustomer) {
        this.id = id;
        this.product = product;
        this.amount = amount;
        this.price = price;
        this.ordercustomer = ordercustomer;
    }

    public OrderItem(Product product, int amount, double price, OrderCustomer ordercustomer) {
        this(-1, product, amount, price, ordercustomer);
    }

    public double getTotal() {
        return amount * price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public OrderCustomer getOrdercustomer() {
        return ordercustomer;
    }

    public void setOrdercustomer(OrderCustomer ordercustomer) {
        this.ordercustomer = ordercustomer;
    }

    public void addAmount(int amount) {
        this.amount = this.amount + amount;
    }

    @Override
    public String toString() {
        return "OrderItem{" + "id=" + id
                + ", productId=" + product.getId() + ", productName=" + product.getName() + " , productPrice=" + product.getPrice()
                + ", amount=" + amount
                + ", total=" + this.getTotal()
                + '}';
    }

}
