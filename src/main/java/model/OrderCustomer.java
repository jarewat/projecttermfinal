/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Waratchapon Ponpiya
 */
public class OrderCustomer {

    private int id;
    private Customer customer;
    private Date created;
    private double total;
    private double income;
    private double change;
    private double discount;
    private Employee seller;

    private ArrayList<OrderItem> orderItem;

    public OrderCustomer(int id, Customer customer, Date created, double total, double income, double change, double discount, Employee seller) {
        this.id = id;
        this.customer = customer;
        this.created = created;
        this.total = total;
        this.income = income;
        this.change = change;
        this.discount = discount;
        this.seller = seller;
        orderItem = new ArrayList<>();
    }

    public OrderCustomer(double total, double income, double change, double discount, Employee seller) {
        this(-1, null, null, total, income, change, discount, seller);
    }

    public OrderCustomer(Customer customer, double total, double income, double change, double discount, Employee seller) {
        this(-1, customer, null, total, income, change, discount, seller);
    }

    public OrderCustomer(Customer customer, double income, double change, double discount, Employee seller) {
        this(-1, customer, null, 0, income, change, discount, seller);
    }

    public void addOrderItem(int id, Product product, int amount, double price) {
        //checkProduct in iteminOrderItem
        for (int row = 0; row < orderItem.size(); row++) {
            OrderItem i = orderItem.get(row);
            if (i.getProduct().getId() == product.getId()) {
                i.addAmount(amount);
                return;
            }
        }
        orderItem.add(new OrderItem(id, product, amount, price, this));

    }

    public void addOrderItem(Product product, int amount) {
        addOrderItem(-1, product, amount, product.getPrice());

    }

    public void deleteOrderItem(int row) {
        orderItem.remove(row);
    }

    public void deleteOderItemAll() {
        orderItem.clear();
    }

    public double getAmount() {
        int amount = 0;

        for (OrderItem i : orderItem) {
            amount = amount + i.getAmount();
        }
        return amount;
    }

    public double getTotalofOrder() {
        return total;
    }

    public double getTotal() {
        double total = 0;
        for (OrderItem i : orderItem) {
            total = total + i.getTotal();
        }
        return total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public Employee getSeller() {
        return seller;
    }

    public void setSeller(Employee seller) {
        this.seller = seller;
    }

    

    public ArrayList<OrderItem> getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(ArrayList<OrderItem> orderItem) {
        this.orderItem = orderItem;
    }

    @Override
    public String toString() {
        String str = "OrderCustomer{" + "id=" + id + ", created=" + created + ", total=" + total + ", income=" + income + ", change=" + change + ", discount=" + discount + ", customerId=" + customer.getId() + ", customerName=" + customer.getName() + ", sellerId=" + seller.getId() + ", sellerName=" + seller.getName() + "\norderItem=";

        for (OrderItem i : orderItem) {
            str = str + i.toString() + "\n";

        }
        str += " }";
        return str;
    }

}
