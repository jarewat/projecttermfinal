/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author aomza
 */
public class Customer {

    private int id;
    private String name;
    private String phone;
    private int point;

    public Customer(int id, String name, String phone, int point) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.point = point;
    }

    public Customer(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Customer(String name, String phone) {
        this.phone = phone;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + name + ", phone=" + phone + ", point=" + point + '}';
    }

}
