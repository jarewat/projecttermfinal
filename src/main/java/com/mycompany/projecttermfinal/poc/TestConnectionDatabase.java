/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projecttermfinal.poc;

import database.Database;
import java.sql.Connection;



/**
 *
 * @author jarew
 */
public class TestConnectionDatabase {
    public static void main(String[] args) {
//        connect database
        Database db = Database.getInstance();
        
//        get connect database
        Connection con = db.getConnection();
        
//        close database
        db.close();
    }
}
